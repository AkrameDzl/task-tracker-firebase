import React, { useContext, useState, useEffect } from "react"
import { auth } from "../firebase"
const AuthContext = React.createContext()
export function useAuth() {
  return useContext(AuthContext)
}
export function AuthProvider({ children }) {
  function singup(email, password) {
    return auth.creatUserWithEmailAndPassword(email, password)
  }
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      setCurrentUser(user)
    })
    return unsubscribe
  }, [])
  const [currentUser, setCurrentUser] = useState()
  const value = {
    currentUser,
    singup,
  }
  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}
